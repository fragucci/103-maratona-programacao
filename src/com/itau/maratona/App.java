package com.itau.maratona;

import java.util.ArrayList;
import java.util.List;

public class App {

	public static void main(String[] args) {
		
		Arquivo arquivo = new Arquivo("src/alunos.csv");
		List<Aluno> alunos = arquivo.ler();
		
		List<Equipe> equipes = new ArrayList<Equipe>();
		
		Sorteador sortear = new Sorteador(alunos,3);
		equipes = sortear.sortear();
		
		Impressora.imprimir(equipes.toString());
		
	}

}
