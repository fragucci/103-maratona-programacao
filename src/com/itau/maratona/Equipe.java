package com.itau.maratona;

import java.util.ArrayList;

public class Equipe {
		
		int id;
		public ArrayList<Aluno> alunos = new ArrayList<Aluno>();
		
		public Equipe(int id) {
			this.id = id;
		}
		
		public void adicionarMenbro (Aluno aluno) {
			alunos.add(aluno);
		}

		@Override
		public String toString() {
			return "Equipe id=" + id + ", alunos=" + alunos + "\n";
		}

}
