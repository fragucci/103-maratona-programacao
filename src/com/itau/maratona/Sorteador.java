package com.itau.maratona;

import java.util.ArrayList;
import java.util.List;

public class Sorteador {

	private List<Aluno> alunos = new ArrayList<Aluno>();
	private int tamanhoEquipe;

	public Sorteador(List<Aluno> alunos, int tamanhoGrupo) {
		this.alunos = alunos;
		this.tamanhoEquipe = tamanhoGrupo;
	}
	
	public List<Equipe> sortear () {
		List<Equipe> equipes = new ArrayList<Equipe>();
		int idLista = 1;
		
		while (!alunos.isEmpty()) {
			Equipe equipe = new Equipe(idLista);
			
			for (int j = 0; j < tamanhoEquipe; j++) {
				int indice = (int) Math.floor((Math.random() * alunos.size()));
				equipe.adicionarMenbro(this.alunos.get(indice));
				alunos.remove(indice);
			}
			equipes.add(equipe);
			
			idLista ++;
		}	
		return equipes;
	}	
}
